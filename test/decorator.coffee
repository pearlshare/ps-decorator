_ = require('lodash')
chai = require('chai')
expect = chai.expect

Decorator = require('../index')

origData = 
  name: 'Oliver Brooks'
  dontShow: 'superSecret'
  sillyNameForDescription: 'Writing coffeescript at the moment!'
  createdAt: new Date()
  ohNoAFunction: () -> "Don't want functions in our output do we?"
  contactDetails: [
    {
      type: 'phone'
      value: 'myPhoneNumber'
    }
    {
      type: 'email'
      value: 'myEmailAddress'
    }
  ]
  otherArray: [
    'fish'
    1
    pig = () -> 'function output'
    {object: 'pig'}
  ]

out = null

describe 'Decorator', ->

  describe 'global values', ->

    it 'should set the restrictedKeys', (done) ->
      Decorator.restrictedKeys = ['bob']
      decorator = new Decorator
      expect(decorator.restrictedKeys).to.include('bob')
      done()

    it 'should set the translations', (done) ->
      Decorator.translations = {'fish': 'face'}
      decorator = new Decorator
      expect(Object.keys(decorator.translations)).to.include('fish')
      done()


    it 'should set the valueTransforms', (done) ->
      Decorator.valueTransforms =
        transformKey: () -> 'new output'

      decorator = new Decorator
      expect(Object.keys(decorator.valueTransforms)).to.include('transformKey')
      done()

  describe 'decorate', ->
    testData = null

    beforeEach (done) ->
      testData = _.clone(origData)
      done()


    context 'Default decorator', ->

      decorator = new Decorator

      it 'should strip out keys which have functions for values', (done) ->
        out = decorator.decorate(testData)
        expect(out.ohNoAFunction).to.equal(undefined)
        done()

      it 'should return undefined if no object is given', (done) ->
        out = decorator.decorate()
        expect(out).to.equal(undefined)
        done()

    context 'With restricted Keys', ->

      out = null

      before (done) ->
        decorator = new Decorator(restrictedKeys: ['dontShow'])
        out = decorator.decorate(testData)
        done()

      it 'should not contain the dontshow key', (done) ->
        expect(out.dontShow).to.equal(undefined)
        done()

      it 'should not remove any other keys', (done) ->
        expect(out.name).to.equal(origData.name)
        done()

    context 'With Translations', ->

      out = null

      before (done) ->
        decorator = new Decorator(translations: {sillyNameForDescription: 'description'})
        out = decorator.decorate(testData)
        done()

      it 'should not contain the dontshow key', (done) ->
        expect(out.description).to.equal(origData.sillyNameForDescription)
        done()

      it 'should not change other keys', (done) ->
        expect(out.name).to.equal(origData.name)
        done()

    context 'With value transforms', (done) ->

      out = null

      before (done) ->
        decorator = new Decorator
          valueTransforms:
            createdAt: (v) -> v.getTime()

        out = decorator.decorate(testData)
        done()

      it 'should tranform the created at date to a time stamp', (done) ->
        expect(out.createdAt).to.equal(origData.createdAt.getTime())
        done()

      it 'should not change other values', (done) ->
        expect(out.name).to.equal(origData.name)
        done()

    context 'nested arrays', ->

      decorator = null

      before (done) ->
        decorator = new Decorator(restrictedKeys: ['type'])
        done()

      it 'should remove type from the nested objects', (done) ->
        out = decorator.decorate(testData)
        expect(out.contactDetails[0]).to.not.have.key('type')
        done()

      it 'should remove functions', (done) ->
        out = decorator.decorate(testData)
        expect(out.otherArray).to.have.length(3)
        done()

      it 'should remove functions', (done) ->
        out = decorator.decorate(testData)
        expect(out.otherArray).to.have.length(3)
        done()


